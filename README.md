# FetchResource

FetchResource is a wrapper around the browesrs [fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API)
that let's you take care of serializing and formatting request and response data in one place and provide a simple API
to deal with JSON RESTFul resources.

# Defining a resource

A resource is an instance of the Resoruce class. Each resource needs:

* An endpoint
* An id property
* A serializer, which takes your formatted data and converts it into
  the format the endpoint is expecting it to be in.
* A formatter, which takes care of formatting your data into your
  desired format


```typescript
import Resource from "fetchResource";
interface Recipe {
  id: number;
  title: string;
  steps: string[]
}
// The serialized recipe is the same here,
// but it can be different.
interface SerializedRecipe extends Recipe {}

async function serializer(recipe: SerializedRecipe) {
  // You can manipulate the data sent to the server here.
  // When you don't need to change the format, returning the data is
  // enough.
  return recipe;
}

function formatter(data: any): Recipe {
  // Transform the data from the server here.
  return {
    id: data.id,
    title: data.title,
    steps: data.steps
  };
}
const resource = new Resource<Recipe, SerializedRecipe>(
  "https://example.com/api/recipes",
  "id",
  serializer,
  formatter
);
```

## Indexing data

Now your resource is defined and you're ready to go. Loading all data
can be done by using the index() method, which does a **GET https://example.com/api/recipes** request

The data is expected to be JSON, and all of the items are expected to be under
the `data` property.


```typescript
// The recipes is now an array of all available recipes.
const recipes = await resource.index()

```

## Getting one item

A single item can be fetched using the `get` method, which is equal to a **GET https://example.com/api/recipes/1** http call:

```typescript
const recipe = await resource.get(1)
```

An exception will be thrown if the item isn't found.

## Saving data
Items can be saved using the save() method. A PUT request is executed if the data
has a set id property and a POST is done if it isn't available:

```typescript
const recipe = await resource.save(recipe)
```

An exception is thrown if the backend returns an error.

## Removing data

Data can be deleted using the remove method, which executes a DELETE request:

```typescript
await resource.remove(1)
```

An exception is thrown if the server returns an error.

## Setting the base url

If you define multiple resources you probably don't want to set your base url in each one of them. You can call `setBasePath` to set a common base path:

```typescript
import { setBasePath } from "fetchResource";
setBasePath("http://example.com/api/")
// You can now define youre resources without a full url.
const resource = new Resource<Recipe, SerializedRecipe>(
  "recipes",
  "id",
  serializer,
  formatter
);

```

## Adding common headers

You might need to include certain headers on each request, you can added them
using `addHeader`:

```typescript
import { addHeader } from "fetchResource";
addHeader("X-MY-HEADER", "myvalue");

```

## Making custom requests

You can make custom requests to using the `get`, `post` `put` functions. The result is a fetch response.

```typescript
import { get } from "fetchResource";
const result = await get("/api/custom-endpoint")
```
