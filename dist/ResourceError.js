"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * A resource error can occurs whenever we do a request
 * that doesn't return a 2xx response. This error contains the
 * actual response so we can make use of it.
 */
var ResourceError = /** @class */ (function (_super) {
    __extends(ResourceError, _super);
    function ResourceError(error, response, data) {
        var _this = _super.call(this, error) || this;
        _this.message = error;
        _this.response = response;
        _this.data = data;
        return _this;
    }
    return ResourceError;
}(Error));
exports.default = ResourceError;
