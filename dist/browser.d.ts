import Resource, { IFetchOptions, setBasePath, addHeader, removeHeader, post, get, put, patch, remove } from "./resource";
export { IFetchOptions, setBasePath, addHeader, removeHeader, get, post, put, patch, remove, };
export default Resource;
