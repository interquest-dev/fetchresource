"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var node_fetch_1 = require("node-fetch");
var resource_1 = require("./resource");
exports.setBasePath = resource_1.setBasePath;
exports.addHeader = resource_1.addHeader;
exports.removeHeader = resource_1.removeHeader;
exports.post = resource_1.post;
exports.get = resource_1.get;
exports.put = resource_1.put;
exports.remove = resource_1.remove;
var fetchFn = function (path, options) {
    return node_fetch_1.default(path, __assign({}, options));
};
resource_1.setFetchFn(fetchFn);
exports.default = resource_1.default;
