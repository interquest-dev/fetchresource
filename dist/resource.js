"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var ResourceError_1 = require("./ResourceError");
// Make it possible to set the base path for all endpoints.
var basePath = "";
// Stores headers that are always sent.
var headers = {};
var fetchFn = function (path, options) {
    return fetch(path, __assign({ credentials: "include" }, options));
};
/**
 * Set the base path, with trailing slash.
 */
function setBasePath(newPath) {
    basePath = newPath;
}
exports.setBasePath = setBasePath;
function setFetchFn(fn) {
    fetchFn = fn;
}
exports.setFetchFn = setFetchFn;
/**
 * Add a global header that is used in all requests.
 */
function addHeader(name, value) {
    headers[name] = value;
}
exports.addHeader = addHeader;
/**
 * Remove a global header.
 */
function removeHeader(name) {
    delete headers[name];
}
exports.removeHeader = removeHeader;
function encodeGetParams(params) {
    var encoded = [];
    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            var value = params[key];
            if (typeof value === "boolean") {
                value = value ? 1 : 0;
            }
            encoded.push(key + "=" + encodeURIComponent(value.toString()));
        }
    }
    return encoded.join("&");
}
var defaultHeaders = {
    Accept: "application/json",
    "Content-Type": "application/json",
};
/**
 * Do a post request to the api using the base path and the provided
 * headers.
 */
function post(path, body, requestHeaders) {
    if (requestHeaders === void 0) { requestHeaders = defaultHeaders; }
    return __awaiter(this, void 0, void 0, function () {
        var response;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    path = basePath + path;
                    return [4 /*yield*/, fetchFn(path, {
                            body: requestHeaders["Content-Type"] &&
                                requestHeaders["Content-Type"] === "application/json"
                                ? JSON.stringify(body)
                                : body,
                            headers: __assign(__assign({}, headers), requestHeaders),
                            method: "POST",
                        })];
                case 1:
                    response = _a.sent();
                    return [2 /*return*/, response];
            }
        });
    });
}
exports.post = post;
/**
 * Do a get request to the api using the base path and the provided
 * headers.
 */
function get(path) {
    return __awaiter(this, void 0, void 0, function () {
        var response;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    path = basePath + path;
                    return [4 /*yield*/, fetchFn(path, {
                            headers: __assign(__assign({}, headers), { Accept: "application/json", "Content-Type": "application/json" }),
                            method: "GET",
                        })];
                case 1:
                    response = _a.sent();
                    return [2 /*return*/, response];
            }
        });
    });
}
exports.get = get;
/**
 * Do a put request to the api using the base path and the provided
 * headers.
 */
function put(path, body, requestHeaders) {
    if (requestHeaders === void 0) { requestHeaders = defaultHeaders; }
    return __awaiter(this, void 0, void 0, function () {
        var response;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    path = basePath + path;
                    return [4 /*yield*/, fetchFn(path, {
                            body: requestHeaders["Content-Type"] &&
                                requestHeaders["Content-Type"] === "application/json"
                                ? JSON.stringify(body)
                                : body,
                            headers: __assign(__assign({}, headers), requestHeaders),
                            method: "PUT",
                        })];
                case 1:
                    response = _a.sent();
                    return [2 /*return*/, response];
            }
        });
    });
}
exports.put = put;
/**
 * Do a patch request to the api using the base path and the provided
 * headers.
 */
function patch(path, body, requestHeaders) {
    if (requestHeaders === void 0) { requestHeaders = defaultHeaders; }
    return __awaiter(this, void 0, void 0, function () {
        var response;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    path = basePath + path;
                    return [4 /*yield*/, fetchFn(path, {
                            body: requestHeaders["Content-Type"] &&
                                requestHeaders["Content-Type"] === "application/json"
                                ? JSON.stringify(body)
                                : body,
                            headers: __assign(__assign({}, headers), requestHeaders),
                            method: "PATCH",
                        })];
                case 1:
                    response = _a.sent();
                    return [2 /*return*/, response];
            }
        });
    });
}
exports.patch = patch;
/**
 * Do a delete request to the api using the base path and the provided
 * headers.
 */
function remove(path) {
    return __awaiter(this, void 0, void 0, function () {
        var response;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    path = basePath + path;
                    return [4 /*yield*/, fetchFn(path, {
                            headers: __assign(__assign({}, headers), { Accept: "application/json", "Content-Type": "application/json" }),
                            method: "DELETE",
                        })];
                case 1:
                    response = _a.sent();
                    return [2 /*return*/, response];
            }
        });
    });
}
exports.remove = remove;
/**
 * This is the main resource class. this is just a thin wrapper
 * around the built-in fetch api in browers. the responses are
 * assumed to match the JSON API specification.
 */
var Resource = /** @class */ (function () {
    function Resource(path, idAttribute, serializer, formatter) {
        this.path = path;
        this.idAttribute = idAttribute;
        this.serializer = serializer;
        this.formatter = formatter;
        this.callbacks = [];
    }
    Resource.error = function (response) {
        return __awaiter(this, void 0, void 0, function () {
            var errorData, message, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(response.status < 500 && response.status !== 404)) return [3 /*break*/, 5];
                        errorData = null;
                        message = "";
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, response.json()];
                    case 2:
                        errorData = _a.sent();
                        message = errorData.message;
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        if (response.statusText) {
                            message = response.statusText;
                        }
                        errorData = {};
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/, new ResourceError_1.default(message, response, errorData)];
                    case 5: return [2 /*return*/, new ResourceError_1.default("", response)];
                }
            });
        });
    };
    Resource.prototype.setPath = function (path) {
        this.path = path;
    };
    Resource.prototype.index = function (parameters) {
        return __awaiter(this, void 0, void 0, function () {
            var path, response, data;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        path = "" + basePath + this.path;
                        if (parameters) {
                            path += "?" + encodeGetParams(parameters);
                        }
                        return [4 /*yield*/, fetchFn(path, {
                                headers: __assign(__assign({}, headers), { Accept: "application/json", "Content-Type": "application/json" }),
                                method: "GET",
                            })];
                    case 1:
                        response = _a.sent();
                        this.response = response;
                        if (!(response.status !== 200)) return [3 /*break*/, 3];
                        return [4 /*yield*/, Resource.error(response)];
                    case 2: throw _a.sent();
                    case 3: return [4 /*yield*/, response.json()];
                    case 4:
                        data = _a.sent();
                        this.data = data;
                        return [2 /*return*/, data.data.map(function (item) {
                                return _this.formatter(item);
                            })];
                }
            });
        });
    };
    Resource.prototype.link = function (link) {
        return __awaiter(this, void 0, void 0, function () {
            var response, data;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, fetchFn(link, {
                            headers: __assign(__assign({}, headers), { Accept: "application/json", "Content-Type": "application/json" }),
                            method: "GET",
                        })];
                    case 1:
                        response = _a.sent();
                        this.response = response;
                        if (!(response.status !== 200)) return [3 /*break*/, 3];
                        return [4 /*yield*/, Resource.error(response)];
                    case 2: throw _a.sent();
                    case 3: return [4 /*yield*/, response.json()];
                    case 4:
                        data = _a.sent();
                        this.data = data;
                        return [2 /*return*/, data.data.map(function (item) {
                                return _this.formatter(item);
                            })];
                }
            });
        });
    };
    Resource.prototype.get = function (id, parameters) {
        return __awaiter(this, void 0, void 0, function () {
            var path, response, data, item, formattedItem;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        path = "" + basePath + this.path + "/" + id;
                        if (parameters) {
                            path += "?" + encodeGetParams(parameters);
                        }
                        return [4 /*yield*/, fetchFn(path, {
                                headers: __assign(__assign({}, headers), { Accept: "application/json", "Content-Type": "application/json" }),
                                method: "GET",
                            })];
                    case 1:
                        response = _a.sent();
                        this.response = response;
                        if (!(response.status !== 200)) return [3 /*break*/, 3];
                        return [4 /*yield*/, Resource.error(response)];
                    case 2: throw _a.sent();
                    case 3: return [4 /*yield*/, response.json()];
                    case 4:
                        data = _a.sent();
                        item = data.data;
                        this.data = data;
                        formattedItem = this.formatter(item);
                        this.callbacks.forEach(function (callback) { return callback(formattedItem, _this); });
                        return [2 /*return*/, formattedItem];
                }
            });
        });
    };
    Resource.prototype.onGet = function (callback) {
        this.callbacks.push(callback);
    };
    Resource.prototype.request = function (method, path, parameters, body) {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        path = path && path.length > 0 ? this.path + "/" + path : this.path;
                        path = basePath + path;
                        if (parameters) {
                            path += "?" + encodeGetParams(parameters);
                        }
                        return [4 /*yield*/, fetchFn(path, {
                                body: body ? JSON.stringify(body) : undefined,
                                headers: __assign(__assign({}, headers), { Accept: "application/json", "Content-Type": "application/json" }),
                                method: method,
                            })];
                    case 1:
                        response = _a.sent();
                        this.response = response;
                        if (!(response.status <= 299)) return [3 /*break*/, 2];
                        return [2 /*return*/, response];
                    case 2: return [4 /*yield*/, Resource.error(response)];
                    case 3: throw _a.sent();
                }
            });
        });
    };
    Resource.prototype.relationships = function () {
        if (this.data && this.data.relationships) {
            return this.data.relationships;
        }
        return {};
    };
    Resource.prototype.links = function () {
        if (this.data && this.data.links) {
            return this.data.links;
        }
        return {};
    };
    Resource.prototype.meta = function () {
        if (this.data && this.data.meta) {
            return this.data.meta;
        }
        return {};
    };
    Resource.prototype.remove = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var path, response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        path = "" + basePath + this.path + "/" + id;
                        return [4 /*yield*/, fetchFn(path, {
                                headers: __assign(__assign({}, headers), { Accept: "application/json", "Content-Type": "application/json" }),
                                method: "DELETE",
                            })];
                    case 1:
                        response = _a.sent();
                        this.response = response;
                        if (!(response.status !== 204)) return [3 /*break*/, 3];
                        return [4 /*yield*/, Resource.error(response)];
                    case 2: throw _a.sent();
                    case 3: return [2 /*return*/, response];
                }
            });
        });
    };
    Resource.prototype.save = function (data, parameters) {
        return __awaiter(this, void 0, void 0, function () {
            var method, path, id, response, _a, _b, _c, _d, _e, fetchedData, item;
            return __generator(this, function (_f) {
                switch (_f.label) {
                    case 0:
                        path = basePath;
                        if (data[this.idAttribute]) {
                            id = data[this.idAttribute];
                            method = "PUT";
                            path += this.path + "/" + id;
                        }
                        else {
                            method = "POST";
                            path += "" + this.path;
                        }
                        if (parameters) {
                            path += "?" + encodeGetParams(parameters);
                        }
                        _a = fetchFn;
                        _b = [path];
                        _c = {};
                        _e = (_d = JSON).stringify;
                        return [4 /*yield*/, this.serializer(data)];
                    case 1: return [4 /*yield*/, _a.apply(void 0, _b.concat([(_c.body = _e.apply(_d, [_f.sent()]),
                                _c.headers = __assign(__assign({}, headers), { Accept: "application/json", "Content-Type": "application/json" }),
                                _c.method = method,
                                _c)]))];
                    case 2:
                        response = _f.sent();
                        this.response = response;
                        if (!((response.status === 201 && method === "POST") ||
                            response.status === 200)) return [3 /*break*/, 4];
                        return [4 /*yield*/, response.json()];
                    case 3:
                        fetchedData = _f.sent();
                        item = fetchedData.data;
                        this.data = fetchedData;
                        return [2 /*return*/, this.formatter(item)];
                    case 4: return [4 /*yield*/, Resource.error(response)];
                    case 5: throw _f.sent();
                }
            });
        });
    };
    Resource.prototype.post = function (path, data) {
        return __awaiter(this, void 0, void 0, function () {
            var response, _a, _b, _c, _d, _e;
            return __generator(this, function (_f) {
                switch (_f.label) {
                    case 0:
                        path = path && path.length > 0 ? this.path + "/" + path : this.path;
                        path = basePath + path;
                        _a = fetchFn;
                        _b = [path];
                        _c = {};
                        _e = (_d = JSON).stringify;
                        return [4 /*yield*/, this.serializer(data)];
                    case 1: return [4 /*yield*/, _a.apply(void 0, _b.concat([(_c.body = _e.apply(_d, [_f.sent()]),
                                _c.headers = __assign(__assign({}, headers), { Accept: "application/json", "Content-Type": "application/json" }),
                                _c.method = "POST",
                                _c)]))];
                    case 2:
                        response = _f.sent();
                        this.response = response;
                        if (!(response.status === 201)) return [3 /*break*/, 3];
                        return [2 /*return*/, response];
                    case 3: return [4 /*yield*/, Resource.error(response)];
                    case 4: throw _f.sent();
                }
            });
        });
    };
    Resource.prototype.action = function (name, data, id, method) {
        if (id === void 0) { id = null; }
        if (method === void 0) { method = "POST"; }
        return __awaiter(this, void 0, void 0, function () {
            var path, response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        path = basePath + this.path;
                        if (id) {
                            path += "/" + id + "/";
                        }
                        path += name;
                        return [4 /*yield*/, fetchFn(path, {
                                body: data ? JSON.stringify(data) : null,
                                headers: __assign(__assign({}, headers), { Accept: "application/json", "Content-Type": "application/json" }),
                                method: method,
                            })];
                    case 1:
                        response = _a.sent();
                        this.response = response;
                        if (response.status < 300) {
                            return [2 /*return*/, response];
                        }
                        return [4 /*yield*/, Resource.error(response)];
                    case 2: throw _a.sent();
                }
            });
        });
    };
    Resource.prototype.getLastResponse = function () {
        return this.response;
    };
    return Resource;
}());
exports.default = Resource;
