import { Response as NodeResponse } from "node-fetch";
import ResourceError from "./ResourceError";
declare type Methods = "GET" | "POST" | "PUT" | "DELETE" | "PATCH";
declare type ResourceResponse = Response | NodeResponse;
export interface IFetchOptions {
    headers: {
        [name: string]: string;
    };
    body?: any;
    method: Methods;
}
declare type FetchFn = (path: string, options: IFetchOptions) => Promise<ResourceResponse>;
interface IGetParameters {
    [key: string]: string | number | boolean;
}
interface IRelationship {
    data: any;
}
interface IRelationships {
    [key: string]: IRelationship;
}
interface ILinks {
    [name: string]: string;
}
interface IMeta {
    [name: string]: any;
}
declare type Callback<Type> = (type: Type, resource: Resource<Type, unknown>) => void;
/**
 * Set the base path, with trailing slash.
 */
export declare function setBasePath(newPath: string): void;
export declare function setFetchFn(fn: FetchFn): void;
/**
 * Add a global header that is used in all requests.
 */
export declare function addHeader(name: string, value: string): void;
/**
 * Remove a global header.
 */
export declare function removeHeader(name: string): void;
/**
 * Do a post request to the api using the base path and the provided
 * headers.
 */
export declare function post(path: string, body: any, requestHeaders?: Record<string, string>): Promise<Response | NodeResponse>;
/**
 * Do a get request to the api using the base path and the provided
 * headers.
 */
export declare function get(path: string): Promise<Response | NodeResponse>;
/**
 * Do a put request to the api using the base path and the provided
 * headers.
 */
export declare function put(path: string, body: any, requestHeaders?: Record<string, string>): Promise<Response | NodeResponse>;
/**
 * Do a patch request to the api using the base path and the provided
 * headers.
 */
export declare function patch(path: string, body: any, requestHeaders?: Record<string, string>): Promise<Response | NodeResponse>;
/**
 * Do a delete request to the api using the base path and the provided
 * headers.
 */
export declare function remove(path: string): Promise<Response | NodeResponse>;
/**
 * This is the main resource class. this is just a thin wrapper
 * around the built-in fetch api in browers. the responses are
 * assumed to match the JSON API specification.
 */
export default class Resource<Type, SerializedType> {
    static error(response: ResourceResponse): Promise<ResourceError>;
    private path;
    private idAttribute;
    private serializer;
    private formatter;
    private data;
    private callbacks;
    private response?;
    constructor(path: string, idAttribute: string, serializer: (data: Type) => Promise<SerializedType>, formatter: (data: any) => Type);
    setPath(path: string): void;
    index(parameters?: IGetParameters): Promise<any>;
    link(link: string): Promise<any>;
    get(id: number | string, parameters?: IGetParameters): Promise<Type>;
    onGet(callback: Callback<Type>): void;
    request(method: "GET" | "PUT" | "DELETE" | "POST", path: string, parameters?: IGetParameters, body?: any): Promise<Response | NodeResponse>;
    relationships(): IRelationships;
    links(): ILinks;
    meta(): IMeta;
    remove(id: number | string): Promise<Response | NodeResponse>;
    save(data: Type, parameters?: IGetParameters): Promise<Type>;
    post(path: string, data: any): Promise<Response | NodeResponse>;
    action(name: string, data: any, id?: string | number | null, method?: Methods): Promise<Response | NodeResponse>;
    getLastResponse(): ResourceResponse | undefined;
}
export {};
