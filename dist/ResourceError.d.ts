import { Response as NodeResponse } from "node-fetch";
/**
 * A resource error can occurs whenever we do a request
 * that doesn't return a 2xx response. This error contains the
 * actual response so we can make use of it.
 */
export default class ResourceError extends Error {
    response: Response | NodeResponse;
    data: any;
    constructor(error: string, response: Response | NodeResponse, data?: any);
}
