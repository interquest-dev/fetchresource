import Resource, { IFetchOptions, setBasePath, addHeader, removeHeader, post, get, put, remove } from "./resource";
export { IFetchOptions, setBasePath, addHeader, removeHeader, post, get, put, remove };
export default Resource;
