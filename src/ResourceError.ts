import { Response as NodeResponse } from "node-fetch";

/**
 * A resource error can occurs whenever we do a request
 * that doesn't return a 2xx response. This error contains the
 * actual response so we can make use of it.
 */
export default class ResourceError extends Error {
  public response: Response | NodeResponse;
  public data: any;
  public constructor(error: string, response: Response | NodeResponse, data?: any) {
    super(error);
    this.message = error;
    this.response = response;
    this.data = data;
  }
}
