import nodeFetch from "node-fetch";
import Resource, {
  IFetchOptions, setBasePath,
  addHeader, removeHeader, setFetchFn, post, get, put, remove
} from "./resource";

export {
  IFetchOptions,
  setBasePath,
  addHeader,
  removeHeader,
  post,
  get,
  put,
  remove
};

const fetchFn = (path: string, options: IFetchOptions) => {
  return nodeFetch(path, {
    ...options,
  });
};

setFetchFn(fetchFn);

export default Resource;
