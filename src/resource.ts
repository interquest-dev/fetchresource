import { Response as NodeResponse } from "node-fetch";
import ResourceError from "./ResourceError";

type Methods = "GET" | "POST" | "PUT" | "DELETE" | "PATCH";
type ResourceResponse = Response | NodeResponse;

export interface IFetchOptions {
  headers: { [name: string]: string };
  body?: any;
  method: Methods;
}

type FetchFn = (
  path: string,
  options: IFetchOptions
) => Promise<ResourceResponse>;

interface IGetParameters {
  [key: string]: string | number | boolean;
}

interface IRelationship {
  data: any;
}

interface IRelationships {
  [key: string]: IRelationship;
}

interface ILinks {
  [name: string]: string;
}

interface IMeta {
  [name: string]: any;
}

type Callback<Type> = (type: Type, resource: Resource<Type, unknown>) => void;

// Make it possible to set the base path for all endpoints.
let basePath = "";

// Stores headers that are always sent.
const headers = {};

let fetchFn: FetchFn = (path: string, options: IFetchOptions) => {
  return fetch(path, {
    credentials: "include",
    ...options,
  });
};
/**
 * Set the base path, with trailing slash.
 */
export function setBasePath(newPath: string) {
  basePath = newPath;
}

export function setFetchFn(fn: FetchFn) {
  fetchFn = fn;
}

/**
 * Add a global header that is used in all requests.
 */
export function addHeader(name: string, value: string) {
  headers[name] = value;
}

/**
 * Remove a global header.
 */
export function removeHeader(name: string) {
  delete headers[name];
}

function encodeGetParams(params: IGetParameters): string {
  const encoded: string[] = [];
  for (const key in params) {
    if (params.hasOwnProperty(key)) {
      let value = params[key];
      if (typeof value === "boolean") {
        value = value ? 1 : 0;
      }
      encoded.push(key + "=" + encodeURIComponent(value.toString()));
    }
  }
  return encoded.join("&");
}

const defaultHeaders = {
  Accept: "application/json",
  "Content-Type": "application/json",
};

/**
 * Do a post request to the api using the base path and the provided
 * headers.
 */
export async function post(
  path: string,
  body: any,
  requestHeaders: Record<string, string> = defaultHeaders
) {
  path = basePath + path;
  const response = await fetchFn(path, {
    body:
      requestHeaders["Content-Type"] &&
      requestHeaders["Content-Type"] === "application/json"
        ? JSON.stringify(body)
        : body,
    headers: {
      ...headers,
      ...requestHeaders,
    },
    method: "POST",
  });
  return response;
}

/**
 * Do a get request to the api using the base path and the provided
 * headers.
 */
export async function get(path: string) {
  path = basePath + path;
  const response = await fetchFn(path, {
    headers: {
      ...headers,
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    method: "GET",
  });
  return response;
}

/**
 * Do a put request to the api using the base path and the provided
 * headers.
 */
export async function put(
  path: string,
  body: any,
  requestHeaders: Record<string, string> = defaultHeaders
) {
  path = basePath + path;
  const response = await fetchFn(path, {
    body:
      requestHeaders["Content-Type"] &&
      requestHeaders["Content-Type"] === "application/json"
        ? JSON.stringify(body)
        : body,
    headers: {
      ...headers,
      ...requestHeaders,
    },
    method: "PUT",
  });
  return response;
}

/**
 * Do a patch request to the api using the base path and the provided
 * headers.
 */
export async function patch(
  path: string,
  body: any,
  requestHeaders: Record<string, string> = defaultHeaders
) {
  path = basePath + path;
  const response = await fetchFn(path, {
    body:
      requestHeaders["Content-Type"] &&
      requestHeaders["Content-Type"] === "application/json"
        ? JSON.stringify(body)
        : body,
    headers: {
      ...headers,
      ...requestHeaders,
    },
    method: "PATCH",
  });
  return response;
}

/**
 * Do a delete request to the api using the base path and the provided
 * headers.
 */
export async function remove(path: string) {
  path = basePath + path;
  const response = await fetchFn(path, {
    headers: {
      ...headers,
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    method: "DELETE",
  });
  return response;
}

/**
 * This is the main resource class. this is just a thin wrapper
 * around the built-in fetch api in browers. the responses are
 * assumed to match the JSON API specification.
 */
export default class Resource<Type, SerializedType> {
  public static async error(response: ResourceResponse) {
    if (response.status < 500 && response.status !== 404) {
      let errorData: any = null;
      let message: string = "";
      try {
        errorData = await response.json();
        message = errorData.message;
      } catch (e) {
        if (response.statusText) {
          message = response.statusText;
        }
        errorData = {};
      }

      return new ResourceError(message, response, errorData);
    }
    return new ResourceError("", response);
  }

  private path: string;
  private idAttribute: string;
  private serializer: (data: Type) => Promise<SerializedType>;
  private formatter: (data: any) => Type;
  private data: any;
  private callbacks: Array<Callback<Type>>;
  private response?: ResourceResponse;

  public constructor(
    path: string,
    idAttribute: string,
    serializer: (data: Type) => Promise<SerializedType>,
    formatter: (data: any) => Type
  ) {
    this.path = path;
    this.idAttribute = idAttribute;
    this.serializer = serializer;
    this.formatter = formatter;
    this.callbacks = [];
  }

  public setPath(path: string) {
    this.path = path;
  }

  public async index(parameters?: IGetParameters) {
    let path = `${basePath}${this.path}`;
    if (parameters) {
      path += "?" + encodeGetParams(parameters);
    }
    const response = await fetchFn(path, {
      headers: {
        ...headers,
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "GET",
    });
    this.response = response;
    if (response.status !== 200) {
      throw await Resource.error(response);
    }

    const data = await response.json();
    this.data = data;
    return data.data.map((item: { [key: string]: any }) => {
      return this.formatter(item);
    });
  }

  public async link(link: string) {
    const response = await fetchFn(link, {
      headers: {
        ...headers,
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "GET",
    });
    this.response = response;
    if (response.status !== 200) {
      throw await Resource.error(response);
    }

    const data = await response.json();
    this.data = data;

    return data.data.map((item: { [key: string]: any }) => {
      return this.formatter(item);
    });
  }

  public async get(id: number | string, parameters?: IGetParameters) {
    let path = `${basePath}${this.path}/${id}`;
    if (parameters) {
      path += "?" + encodeGetParams(parameters);
    }
    const response = await fetchFn(path, {
      headers: {
        ...headers,
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "GET",
    });
    this.response = response;
    if (response.status !== 200) {
      throw await Resource.error(response);
    }
    const data = await response.json();
    const item = data.data as { [key: string]: any };
    this.data = data;
    const formattedItem = this.formatter(item);
    this.callbacks.forEach((callback) => callback(formattedItem, this));
    return formattedItem;
  }

  public onGet(callback: Callback<Type>) {
    this.callbacks.push(callback);
  }

  public async request(
    method: "GET" | "PUT" | "DELETE" | "POST",
    path: string,
    parameters?: IGetParameters,
    body?: any
  ) {
    path = path && path.length > 0 ? this.path + "/" + path : this.path;

    path = basePath + path;
    if (parameters) {
      path += "?" + encodeGetParams(parameters);
    }
    const response = await fetchFn(path, {
      body: body ? JSON.stringify(body) : undefined,
      headers: {
        ...headers,
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method,
    });
    this.response = response;
    if (response.status <= 299) {
      return response;
    } else {
      throw await Resource.error(response);
    }
  }

  public relationships(): IRelationships {
    if (this.data && this.data.relationships) {
      return this.data.relationships as IRelationships;
    }
    return {};
  }

  public links(): ILinks {
    if (this.data && this.data.links) {
      return this.data.links;
    }
    return {};
  }

  public meta(): IMeta {
    if (this.data && this.data.meta) {
      return this.data.meta;
    }
    return {};
  }

  public async remove(id: number | string) {
    const path = `${basePath}${this.path}/${id}`;
    const response = await fetchFn(path, {
      headers: {
        ...headers,
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "DELETE",
    });
    this.response = response;
    if (response.status !== 204) {
      throw await Resource.error(response);
    }
    return response;
  }

  public async save(data: Type, parameters?: IGetParameters): Promise<Type> {
    let method: "PUT" | "POST";
    let path = basePath;
    if (data[this.idAttribute]) {
      const id = data[this.idAttribute];
      method = "PUT";
      path += `${this.path}/${id}`;
    } else {
      method = "POST";
      path += `${this.path}`;
    }

    if (parameters) {
      path += "?" + encodeGetParams(parameters);
    }
    const response = await fetchFn(path, {
      body: JSON.stringify(await this.serializer(data)),
      headers: {
        ...headers,
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method,
    });
    this.response = response;
    if (
      (response.status === 201 && method === "POST") ||
      response.status === 200
    ) {
      const fetchedData = await response.json();
      const item = fetchedData.data as { [key: string]: any };
      this.data = fetchedData;
      return this.formatter(item);
    } else {
      throw await Resource.error(response);
    }
  }

  public async post(path: string, data: any) {
    path = path && path.length > 0 ? this.path + "/" + path : this.path;
    path = basePath + path;
    const response = await fetchFn(path, {
      body: JSON.stringify(await this.serializer(data)),
      headers: {
        ...headers,
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "POST",
    });
    this.response = response;
    if (response.status === 201) {
      return response;
    } else {
      throw await Resource.error(response);
    }
  }

  public async action(
    name: string,
    data: any,
    id: string | number | null = null,
    method: Methods = "POST"
  ) {
    let path = basePath + this.path;
    if (id) {
      path += "/" + id + "/";
    }
    path += name;
    const response = await fetchFn(path, {
      body: data ? JSON.stringify(data) : null,
      headers: {
        ...headers,
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method,
    });
    this.response = response;
    if (response.status < 300) {
      return response;
    }
    throw await Resource.error(response);
  }

  public getLastResponse(): ResourceResponse | undefined {
    return this.response;
  }
}
