import { FetchMock } from "jest-fetch-mock/types";
import Resource, {
  addHeader,
  post,
  get,
  setBasePath,
  put,
  patch,
  remove,
} from "../src/browser";

const fetch = global.fetch as FetchMock;
// The entry interface represents an entry that
// the resource class converts to.
interface IEntry {
  id: number;
  title: string;
  body: string;
}

// The serializer takes data and converts it to an IEntry.
async function entrySerializer(data: IEntry): Promise<IEntry> {
  return data;
}

// The entry formatter formats the data from the resource
// into something that works for our application.
function entryFormatter(data: any): IEntry {
  return {
    body: data.body,
    id: parseInt(data.id, 10),
    title: data.title,
  };
}

describe("resource api", () => {
  beforeEach(() => {
    fetch.resetMocks();
  });

  function getResource() {
    return new Resource<IEntry, IEntry>(
      "/entries",
      "id",
      entrySerializer,
      entryFormatter
    );
  }

  it("Test that a get request works properly", async () => {
    addHeader("X-Token", "A token");
    const entryResource = getResource();
    const data: IEntry[] = [];
    for (let i = 1; i <= 10; i++) {
      data.push({
        body: "Example body",
        id: i,
        title: "Example title",
      });
    }
    fetch.mockResponseOnce(
      JSON.stringify({
        data,
        meta: { some: "metadata" },
        links: { next: "/entries?page=2" },
      })
    );
    // Assert on the response
    const entries = await entryResource.index({
      page: 1,
      data: "test",
      order: false,
    });
    expect(entries[0].id).toEqual(1);
    expect(entryResource.links().next).toBe("/entries?page=2");
    expect(entryResource.meta().some).toBe("metadata");

    // Assert on the times called and arguments given to fetch
    expect(fetch.mock.calls.length).toEqual(1);
    expect(fetch.mock.calls[0][0]).toEqual("/entries?page=1&data=test&order=0");

    fetch.mockResponseOnce(
      JSON.stringify({
        data,
        links: { next: "/entries?page=3" },
      })
    );

    const nextPage = await entryResource.link(entryResource.links().next);
    expect(nextPage[0].id).toEqual(1);
    expect(fetch.mock.calls[1][0]).toEqual("/entries?page=2");
    fetch.mockResponseOnce(
      JSON.stringify({
        data: {
          body: "Example body",
          id: 1,
          title: "Example title",
        },
      })
    );
    const entry = await entryResource.get(1, { test: "param" });
    expect(entry.id).toEqual(1);
    expect(fetch.mock.calls[2][0]).toContain("test=param");
  });

  it("Get callback", async () => {
    const entryResource = getResource();
    const mockFn = jest.fn(
      (data: IEntry, resource: Resource<IEntry, unknown>) => {}
    );
    entryResource.onGet(mockFn);
    const data: IEntry = {
      body: "Example body",
      id: 1,
      title: "Example title",
    };
    fetch.mockResponseOnce(
      JSON.stringify({
        data,
        relationships: { comments: ["post1", "post2"] },
        meta: { some: "metadata" },
        links: { next: "/entries?page=2" },
      })
    );
    await entryResource.get(1);
    expect(mockFn.mock.calls.length).toBe(1);
    expect(mockFn.mock.calls[0][0]).toHaveProperty("id");
    expect(mockFn.mock.calls[0][1]).toBe(entryResource);
  });

  it("custom get request", async () => {
    fetch.mockResponseOnce(
      JSON.stringify({
        message: "All good",
      })
    );
    setBasePath("http://this-is-a-base.com");
    addHeader("X-extra-header", "value");
    await get("/api/test");
    expect(fetch.mock.calls.length).toEqual(1);
    expect(fetch.mock.calls[0][0]).toEqual(
      "http://this-is-a-base.com/api/test"
    );
    expect(fetch.mock.calls[0][1].headers["X-extra-header"]).toEqual("value");
  });

  it("custom post request", async () => {
    fetch.mockResponseOnce(
      JSON.stringify({
        message: "All good",
      })
    );
    setBasePath("http://this-is-a-base.com");
    addHeader("X-extra-header", "value");
    await post("/api/test", { some: "body" }, { Accept: "text/plain" });
    expect(fetch.mock.calls.length).toEqual(1);
    expect(fetch.mock.calls[0][0]).toEqual(
      "http://this-is-a-base.com/api/test"
    );
    expect(fetch.mock.calls[0][1].headers["X-extra-header"]).toEqual("value");
    expect(fetch.mock.calls[0][1].headers["Accept"]).toEqual("text/plain");
    expect(fetch.mock.calls[0][1].headers["Content-Type"]).not.toBeDefined();
  });

  it("custom put request", async () => {
    fetch.mockResponseOnce(
      JSON.stringify({
        message: "All good",
      })
    );
    setBasePath("http://this-is-a-base.com");
    addHeader("X-extra-header", "value");
    await put("/api/test", { some: "body" }, { Accept: "text/plain" });
    expect(fetch.mock.calls.length).toEqual(1);
    expect(fetch.mock.calls[0][0]).toEqual(
      "http://this-is-a-base.com/api/test"
    );
    expect(fetch.mock.calls[0][1].headers["X-extra-header"]).toEqual("value");
    expect(fetch.mock.calls[0][1].headers["Accept"]).toEqual("text/plain");
  });

  it("custom delete request", async () => {
    fetch.mockResponseOnce(
      JSON.stringify({
        message: "All good",
      })
    );
    setBasePath("http://this-is-a-base.com");
    addHeader("X-extra-header", "value");
    await remove("/api/test");
    expect(fetch.mock.calls.length).toEqual(1);
    expect(fetch.mock.calls[0][0]).toEqual(
      "http://this-is-a-base.com/api/test"
    );
    expect(fetch.mock.calls[0][1].headers["X-extra-header"]).toEqual("value");
    expect(fetch.mock.calls[0][1].method).toEqual("DELETE");
  });

  it("custom patch request", async () => {
    fetch.mockResponseOnce(
      JSON.stringify({
        message: "All good",
      })
    );
    setBasePath("http://this-is-a-base.com");
    addHeader("X-extra-header", "value");
    await patch("/api/test", { some: "body" });
    expect(fetch.mock.calls.length).toEqual(1);
    expect(fetch.mock.calls[0][0]).toEqual(
      "http://this-is-a-base.com/api/test"
    );
    expect(fetch.mock.calls[0][1].headers["X-extra-header"]).toEqual("value");
    expect(fetch.mock.calls[0][1].method).toEqual("PATCH");
  });

  it("Custom request to api endpoint", async () => {
    setBasePath("http://this-is-a-base.com");
    const resource = getResource();
    fetch.mockResponseOnce(
      JSON.stringify({
        message: "All good",
      })
    );
    const result = await resource.request(
      "POST",
      "some-path",
      { a: "parameter" },
      { some: "data" }
    );
    expect(fetch.mock.calls.length).toEqual(1);
    expect(fetch.mock.calls[0][0]).toEqual(
      "http://this-is-a-base.com/entries/some-path?a=parameter"
    );
    expect(fetch.mock.calls[0][1].method).toEqual("POST");
    const data = await result.json();
    expect(data.message).toEqual("All good");
  });
});
